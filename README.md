# openssh

#### 介绍

openharmony系统的openssh支持。

#### 代码下载

```
git clone https://gitee.com/lyn1996/openssh.git
```

#### 软件架构

```
├── openssh-9.8p1.tar.gz    ---- openssh源码
├── patch                   ---- 鸿蒙化需要修改的patch
├── README.en.md            ---- 编译安装方法
└── README.md               ---- 编译安装方法

```

#### 编译环境配置

1. 在你的编译机器上首先配置编译工具链，在Openharmony发布的SDK中有一个ohos文件夹，该文件夹提供了可以在Openharmony上可以使用的编译工具链
2. 准备编译工具make
3. 用以下脚本配置环境变量

```
export OHOS_SDK=/your_path/ohos-sdk
export AS=${OHOS_SDK}/native/llvm/bin/llvm-as
export CC="${OHOS_SDK}/native/llvm/bin/clang --target=aarch64-linux-ohos"
export CXX="${OHOS_SDK}/native/llvm/bin/clang++ --target=aarch64-linux-ohos"
export LD=${OHOS_SDK}/native/llvm/bin/ld.lld
export STRIP=${OHOS_SDK}/native/llvm/bin/llvm-strip
export RANLIB=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
export OBJDUMP=${OHOS_SDK}/native/llvm/bin/llvm-objdump
export OBJCOPY=${OHOS_SDK}/native/llvm/bin/llvm-objcopy
export NM=${OHOS_SDK}/native/llvm/bin/llvm-nm
export AR=${OHOS_SDK}/native/llvm/bin/llvm-ar
export CFLAGS="-fPIC -D__MUSL__=1"
export CXXFLAGS="-fPIC -D__MUSL__=1"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${OHOS_SDK}/native/llvm/lib
```

#### 安装教程
```
# cd /your_path/openssh
# tar zxvf openssh-9.8p1.tar.gz
# cd openssh-9.8p1
# patch -p1 < ../patch/Use_absolute_path_for_pwd.patch
# ./configure --prefix="${PWD}/install" --host=aarch64-linux
# make && make install
```
