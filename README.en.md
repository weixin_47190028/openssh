# openssh

#### Introduce

Support for openssh.

#### Download

```
git clone git@gitee.com:c_naonao/openssh.git
```

#### Software architecture

```
├── openssh-9.8p1.tar.gz    ---- openssh source code
├── patch                   ---- The patches that need to be modified for openssh on Openharmony
├── README.en.md            ---- Compilation and installation methods
└── README.md               ---- Compilation and installation methods
```

#### Environment configuration

1. Configure the compilation toolchain on your compilation machine. In the SDK released by OpenHarmony, there is an ohos folder that provides compilation toolchains that can be used on Openharmony 
2. Prepare make
3. Use the following script to configure environment variables

```
export OHOS_SDK=/your_path/ohos-sdk
export AS=${OHOS_SDK}/native/llvm/bin/llvm-as
export CC="${OHOS_SDK}/native/llvm/bin/clang --target=aarch64-linux-ohos"
export CXX="${OHOS_SDK}/native/llvm/bin/clang++ --target=aarch64-linux-ohos"
export LD=${OHOS_SDK}/native/llvm/bin/ld.lld
export STRIP=${OHOS_SDK}/native/llvm/bin/llvm-strip
export RANLIB=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
export OBJDUMP=${OHOS_SDK}/native/llvm/bin/llvm-objdump
export OBJCOPY=${OHOS_SDK}/native/llvm/bin/llvm-objcopy
export NM=${OHOS_SDK}/native/llvm/bin/llvm-nm
export AR=${OHOS_SDK}/native/llvm/bin/llvm-ar
export CFLAGS="-fPIC -D__MUSL__=1"
export CXXFLAGS="-fPIC -D__MUSL__=1"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${OHOS_SDK}/native/llvm/lib
```

#### INSTALL
```
# cd /your_path/openssh
# tar zxvf openssh-9.8p1.tar.gz
# cd openssh-9.8p1
# patch -p1 < ../patch/Use_absolute_path_for_pwd.patch
# ./configure --prefix="${PWD}/install" --host=aarch64-linux
# make && make install
```